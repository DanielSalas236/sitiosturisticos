package com.colomTours.sitiosTuristicos.controller;


import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.colomTours.sitiosTuristicos.model.Sitio;
import com.colomTours.sitiosTuristicos.util.AbstractTest;

@EnableWebMvc
@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
public class SitiosTest extends AbstractTest{
	private static final  int sturis_id = -1 ;
	private static final  String sturis_nombre = "Prueba";
	private static final  String sturis_ubicacion = "Ubi_Prueba" ;
	private static final  int sturis_nit	 = 1111111111;
	private static final  String sturis_descripcion = "Desc_Prueba" ;
	private static final  String sturis_estado = "1";
	private static final  String sturis_guia = "Prueba.pdf";

	@Test
	@DisplayName("createNewUserTest")
	@Rollback
	public void createNewUserTest() throws Exception {
		String uri = "/sitio";
		Sitio r = new Sitio();
		r.setSturis_id(sturis_id);
		r.setSturis_nombre(sturis_nombre);
		r.setSturis_ubicacion(sturis_ubicacion);
		r.setSturis_nit(sturis_nit);
		r.setSturis_descripcion(sturis_descripcion);
		r.setSturis_estado(sturis_estado);
		r.setSturis_guia(sturis_guia);
		r.setMensaje("holiwi");
		String inputJson = super.mapToJson(r);
//		mvc.perform(
//				MockMvcRequestBuilders.post(uri).param("id", "1254")).andExpect(status().isOk()).andExpect(model().attribute(
//						"id", is("1254"))).andDo(print());
		mvc.perform(
				MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
					.andReturn();
	}
	
	@Test
	@DisplayName("getAllSitiosTest")
	@Rollback
	public void getAllSitiosTest() throws Exception {
		String uri = "/todos";
		MvcResult mvcResult = mvc.perform(
		MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
					.andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

}
