package com.colomTours.sitiosTuristicos.services;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.colomTours.sitiosTuristicos.dao.api.SitioJDBCDaoAPI;
import com.colomTours.sitiosTuristicos.model.Sitio;
import com.colomTours.sitiosTuristicos.service.api.SitioServiceAPI;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class SitiosServiceTest {

	private static final  int sturis_id = -1 ;
	private static final  String sturis_nombre = "Prueba";
	private static final  String sturis_ubicacion = "Ubi_Prueba" ;
	private static final  int sturis_nit	 = 1111111111;
	private static final  String sturis_descripcion = "Desc_Prueba" ;
	private static final  String sturis_estado = "1";
	private static final  String sturis_guia = "Prueba.pdf";

	
//	@Autowired
//	private SitioJDBCDaoAPI sitioJDBCDaoAPI;
	
	@Autowired
	private SitioServiceAPI sitioServiceAPI;
	
	@BeforeEach
	void before() {
		assertNotNull(sitioServiceAPI);
	}
	
	@Test
	@DisplayName("crearSitioTest")
	@Order(1)
	@Rollback(true)
	public void crearSitioTest() throws Exception {
		Sitio r = new Sitio();
		r.setSturis_id(sturis_id);
		r.setSturis_nombre(sturis_nombre);
		r.setSturis_ubicacion(sturis_ubicacion);
		r.setSturis_nit(sturis_nit);
		r.setSturis_descripcion(sturis_descripcion);
		r.setSturis_estado(sturis_estado);
		r.setSturis_guia(sturis_guia);
		sitioServiceAPI.save(r);
	}

}
