package com.colomTours.sitiosTuristicos.model;

public class Sitio {

	private int sturis_id ;
	private String sturis_nombre;
	private String sturis_ubicacion;
	private int sturis_nit;
	private String sturis_descripcion;
	private String sturis_estado;
	private String sturis_guia;
	private String mensaje;
	
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public int getSturis_id() {
		return sturis_id;
	}
	public void setSturis_id(int sturis_id) {
		this.sturis_id = sturis_id;
	}
	public String getSturis_nombre() {
		return sturis_nombre;
	}
	public void setSturis_nombre(String sturis_nombre) {
		this.sturis_nombre = sturis_nombre;
	}
	public String getSturis_ubicacion() {
		return sturis_ubicacion;
	}
	public void setSturis_ubicacion(String sturis_ubicacion) {
		this.sturis_ubicacion = sturis_ubicacion;
	}
	public int getSturis_nit() {
		return sturis_nit;
	}
	public void setSturis_nit(int sturis_nit) {
		this.sturis_nit = sturis_nit;
	}
	public String getSturis_descripcion() {
		return sturis_descripcion;
	}
	public void setSturis_descripcion(String sturis_descripcion) {
		this.sturis_descripcion = sturis_descripcion;
	}
	public String getSturis_estado() {
		return sturis_estado;
	}
	public void setSturis_estado(String sturis_estado) {
		this.sturis_estado = sturis_estado;
	}
	public String getSturis_guia() {
		return sturis_guia;
	}
	public void setSturis_guia(String sturis_guia) {
		this.sturis_guia = sturis_guia;
	}
	
	
}
