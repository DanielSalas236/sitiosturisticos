package com.colomTours.sitiosTuristicos.controller;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.colomTours.sitiosTuristicos.commons.ApiResponseClass;
import com.colomTours.sitiosTuristicos.model.Sitio;
import com.colomTours.sitiosTuristicos.service.api.FilesStorageService;
import com.colomTours.sitiosTuristicos.service.api.SitioServiceAPI;
import com.colomTours.sitiosTuristicos.upload.message.ResponseMessage;


@RestController
@RequestMapping(value = "/sitio")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE, RequestMethod.PUT})
public class SitioController {
	
	 
	@Autowired
	private SitioServiceAPI sitioServiceAPI;

	@Autowired
	  FilesStorageService storageService;


	@GetMapping(value = "/todos")
	public ResponseEntity<List<Sitio>> getAll() {
		return new ResponseEntity<List<Sitio>>(sitioServiceAPI.getAll(), HttpStatus.OK);

	}

	@GetMapping(value = "/guardar")
	public ResponseEntity<?> save(@RequestParam Map<String, Object> params) {
		Sitio sitio = new Sitio();
//        sitio.setSturis_id(Integer.valueOf(params.get("id").toString()));
		sitio.setSturis_descripcion(params.get("descripcion").toString());		
		sitio.setSturis_estado(params.get("estado").toString());
		sitio.setSturis_nit(Integer.valueOf((params.get("nit").toString())));
		sitio.setSturis_nombre(params.get("nombre").toString());
		sitio.setSturis_ubicacion(params.get("ubicacion").toString());
		sitioServiceAPI.save(sitio);
		return new ResponseEntity<>(
				new ApiResponseClass("Se guardó el sitio corretamente", "success", HttpStatus.OK), HttpStatus.OK);
	}
	
	@PutMapping(value = "/actualizar")
	public ResponseEntity<?> update(@RequestParam Map<String, Object> params) {
		Sitio sitio = new Sitio();
//        sitio.setSturis_id(Integer.valueOf(params.get("id").toString()));
		sitio.setSturis_descripcion(params.get("descripcion").toString());		
		sitio.setSturis_estado(params.get("estado").toString());
		sitio.setSturis_guia(params.get("guia").toString());
		sitio.setSturis_nit(Integer.valueOf((params.get("nit").toString())));
		sitio.setSturis_nombre(params.get("nombre").toString());
		sitio.setSturis_ubicacion(params.get("ubicacion").toString());
		sitioServiceAPI.update(sitio);
		return new ResponseEntity<>(
				new ApiResponseClass("Se actualizó el sitio corretamente", "success", HttpStatus.OK), HttpStatus.OK);
	}

	@GetMapping(value = "/buscar")
	public List<Sitio> findByNombre(@RequestParam Map<String, Object> params) {
		List<Sitio> result = sitioServiceAPI.findByNombre(params.get("nombre").toString());
		if(result.isEmpty() == true) {
			Sitio st = new Sitio();
			List<Sitio> error = new ArrayList<>();
			st.setMensaje("Sitio no encontrado");
			error.add(st);
			return error;
		}else {
			return result;
		}
	}

	@DeleteMapping(value = "/delete")
	public ResponseEntity<?> delete(@RequestParam Map<String, Object> params) {

		sitioServiceAPI.delete(Integer.valueOf(params.get("id").toString()));

		return new ResponseEntity<>(new ApiResponseClass("Se elimino el sitio corretamente", "info", HttpStatus.OK),
				HttpStatus.OK);
	}
	
	
	
	@PostMapping("/upload")
	  public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile[] file, @RequestParam("nombre") String nombre) {
	    String message = "";
	    nombre =" "+nombre;  
	    try {
	    	
	    	List<Sitio> st = sitioServiceAPI.findByNombre(nombre);
	    	Sitio id_sitio = st.get(0);
	    	int id = id_sitio.getSturis_id();
	    	for(MultipartFile f : file) {
	    		String url = "uploads/"+id+"-"+f.getOriginalFilename();
		        if(f.getContentType().equals("image/svg+xml")) { // image/svg+xml
		        	sitioServiceAPI.save_image(url, id);
			        storageService.save(f, id);
		        }
		        
		        if(f.getContentType().equals("application/pdf")) { // application/pdf
		        	sitioServiceAPI.update_guia(id, url);
			        storageService.save(f, id);
		        }
	    	}
	    	
	      message = "Uploaded the files successfully: ";
	      return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	    } catch (Exception e) {
	      message = "Could not upload the file: "+e;
	      return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
	    }
	  }
	
	

}
