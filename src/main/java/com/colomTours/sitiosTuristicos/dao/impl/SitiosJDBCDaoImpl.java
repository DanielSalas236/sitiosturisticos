package com.colomTours.sitiosTuristicos.dao.impl;

import com.colomTours.sitiosTuristicos.dao.api.SitioJDBCDaoAPI;
import com.colomTours.sitiosTuristicos.model.Sitio;
import com.colomTours.sitiosTuristicos.rowmapper.SitioRowMapper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class SitiosJDBCDaoImpl implements SitioJDBCDaoAPI {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public SitiosJDBCDaoImpl(JdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void save(Sitio sitio) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO SITIO_TUR");
		sql.append(" VALUES(?,?,?,?,?,?,?)");
		
		jdbcTemplate.update(sql.toString(), null, sitio.getSturis_nombre(), sitio.getSturis_ubicacion(), sitio.getSturis_nit(), 
				sitio.getSturis_descripcion(), sitio.getSturis_estado(), sitio.getSturis_guia());
		
	}

	@Override
	public List<Sitio> getAll() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM SITIO_TUR");

		return jdbcTemplate.query(sql.toString(), new SitioRowMapper());
	}

	@Override
	public void delete(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM SITIO_TUR WHERE STURIS_ID ="+id);
		jdbcTemplate.execute(sql.toString());
	}

	@Override
	public void update(Sitio sitio) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE SITIO_TUR SET STURIS_NOMBRE = ?, STURIS_UBICACION = ?, STURIS_NIT = ?, STURIS_DESCRIPCION = ?,"
				+ "STURIS_ESTADO = ?, STURIS_GUIA = ? WHERE STURIS_ID = ?");
		
		jdbcTemplate.update(sql.toString(), sitio.getSturis_nombre(), sitio.getSturis_ubicacion(), sitio.getSturis_nit(), 
				sitio.getSturis_descripcion(), sitio.getSturis_estado(), sitio.getSturis_guia(), sitio.getSturis_id());
		
	}

	@Override
	public List<Sitio> findByNombre(String nombre) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM SITIO_TUR WHERE STURIS_NOMBRE ='"+nombre+"'");
		return jdbcTemplate.query(sql.toString(), new SitioRowMapper());
	}
	
	@Override
	public void save_img(String url, int id_sitio){
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO IMG_SIT_TUR");
		sql.append(" VALUES(?,?,?)");
		jdbcTemplate.update(sql.toString(), null, url, id_sitio );
	}

	@Override
	public void update_guia(int id_sitio, String url) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE SITIO_TUR SET STURIS_GUIA = ? WHERE STURIS_ID = ?");
		
		jdbcTemplate.update(sql.toString(), url, id_sitio);
		
	}
	
}
