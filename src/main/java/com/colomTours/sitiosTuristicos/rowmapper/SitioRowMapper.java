package com.colomTours.sitiosTuristicos.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.colomTours.sitiosTuristicos.model.Sitio;

public class SitioRowMapper implements RowMapper<Sitio> {

	@Override
	public Sitio mapRow(ResultSet rs, int rowNum) throws SQLException {
		Sitio sitio = new Sitio();
		sitio.setSturis_id(rs.getInt("STURIS_ID"));
		sitio.setSturis_nombre(rs.getString("STURIS_NOMBRE"));
		sitio.setSturis_ubicacion(rs.getString("STURIS_UBICACION"));
		sitio.setSturis_nit(rs.getInt("STURIS_NIT"));
		sitio.setSturis_estado(rs.getString("STURIS_ESTADO"));
		sitio.setSturis_guia(rs.getString("STURIS_GUIA"));
		sitio.setSturis_descripcion(rs.getString("STURIS_DESCRIPCION"));
		sitio.setMensaje("Sitio encontrado");
		return sitio;
	}

}
