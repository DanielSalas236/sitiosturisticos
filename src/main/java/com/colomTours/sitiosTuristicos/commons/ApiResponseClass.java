package com.colomTours.sitiosTuristicos.commons;

import org.springframework.http.HttpStatus;

public class ApiResponseClass {

	private String message;
	private String severity;
	private HttpStatus httpStatus;

	public ApiResponseClass(String message, String severity, HttpStatus ok) {
		super();
		this.message = message;
		this.severity = severity;
		this.httpStatus = ok;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

}
